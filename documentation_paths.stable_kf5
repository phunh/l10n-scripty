# The syntax of the file is easy
# A module needs to have a line declaring the module
# Syntax:
#      module modulename
# After that there can be any entry lines
# entry lines point to a directory with single doc for that module
# Syntax:
#      entry dir_name scm_path
# Where
#      scm_path is
#        a kde git repo identifier followed by path_to_folder
#      dir_name is name of the entry e.g. konversation

# workspace
entry kcontrol plasma-desktop doc/kcontrol
entry kcontrol/kgamma5 kgamma5 doc
entry kfontview plasma-desktop doc/kfontview
entry knetattach plasma-desktop doc/knetattach
entry plasma-desktop plasma-desktop doc/plasma-desktop
entry kdesu kde-cli-tools doc/kdesu
entry kcontrol5 kde-cli-tools doc/kcontrol5
entry kcontrol/khotkeys khotkeys doc/kcm
entry kinfocenter kinfocenter doc
entry kmenuedit kmenuedit doc
entry kcontrol/bluedevil bluedevil doc
entry kcontrol/desktop kwin doc/desktop
entry kcontrol/kwindecoration kwin doc/kwindecoration
entry kcontrol/kwinscreenedges kwin doc/kwinscreenedges
entry kcontrol/kwintabbox kwin doc/kwintabbox
entry kcontrol/kwintouchscreen kwin doc/kwintouchscreen
entry kcontrol/kwinvirtualkeyboard kwin doc/kwinvirtualkeyboard
entry kcontrol/windowbehaviour kwin doc/windowbehaviour
entry kcontrol/windowspecific kwin doc/windowspecific
entry kcontrol/kwineffects kwin doc/kwineffects
entry klipper plasma-workspace doc/klipper
entry kcontrol plasma-workspace doc/kcontrol
entry kcontrol/powerdevil powerdevil doc/kcm
entry kcontrol/plasma-pa plasma-pa doc/kcontrol/plasma-pa
entry systemsettings systemsettings doc
entry plasma-sdk_engineexplorer plasma-sdk engineexplorer
entry plasma-sdk_plasmoidviewer plasma-sdk plasmoidviewer

# applications
entry dolphin dolphin doc
entry kate kate doc/kate
entry kwrite kate doc/kwrite
entry katepart kate doc/katepart
entry konsole konsole doc/manual
entry khelpcenter khelpcenter doc/khelpcenter
entry fundamentals khelpcenter doc/fundamentals
entry onlinehelp khelpcenter doc/onlinehelp
entry glossary khelpcenter doc/glossary
entry konqueror konqueror doc/konqueror
entry kcontrol5 konqueror doc/kcontrol5
entry kfind kfind doc
entry keditbookmarks keditbookmarks doc
entry partitionmanager partitionmanager doc

# kdeedu
entry artikulate artikulate doc
entry blinken blinken doc
entry cantor cantor doc
entry kalgebra kalgebra doc
entry kalzium kalzium doc
entry kanagram kanagram doc
entry kbruch kbruch doc
entry kgeography kgeography doc
entry khangman khangman doc
entry kig kig doc
entry kiten kiten doc
entry klettres klettres doc
entry kmplot kmplot doc
entry ktouch ktouch doc
entry kturtle kturtle doc
entry kwordquiz kwordquiz doc
entry marble marble doc
entry minuet minuet doc
entry parley parley docs/parley
entry rocs rocs doc
entry step step doc

# kdegames
entry bomber bomber doc
entry bovo bovo doc
entry granatier granatier doc
entry kajongg kajongg doc
entry kapman kapman doc
entry katomic katomic doc
entry kblackbox kblackbox doc
entry kblocks kblocks doc
entry kbounce kbounce doc
entry kbreakout kbreakout doc
entry kdiamond kdiamond doc
entry kfourinline kfourinline doc
entry kgoldrunner kgoldrunner doc
entry kigo kigo doc
entry killbots killbots doc
entry kiriki kiriki doc
entry kjumpingcube kjumpingcube doc
entry klickety klickety doc
entry klines klines doc
entry kmahjongg kmahjongg doc
entry kmines kmines doc
entry knights knights doc
entry knavalbattle knavalbattle doc
entry knetwalk knetwalk doc
entry kolf kolf doc
entry kollision kollision doc
entry konquest konquest doc
entry kpat kpat doc
entry kreversi kreversi doc
entry kshisen kshisen doc
entry ksirk ksirk doc/ksirk
entry ksirkskineditor ksirk doc/ksirkskineditor
entry ksnakeduel ksnakeduel doc
entry kspaceduel kspaceduel doc
entry ksquares ksquares doc
entry ksudoku ksudoku doc
entry ktuberling ktuberling doc
entry kubrick kubrick doc
entry lskat lskat doc
entry palapeli palapeli doc
entry picmi picmi doc

# kdegraphics
entry kcontrol/kamera kamera doc
entry gwenview gwenview doc
entry kolourpaint kolourpaint doc
entry kruler kruler doc
entry okular okular doc
entry spectacle spectacle doc
entry skanlite skanlite doc

# pim
entry konsolekalendar akonadi-calendar-tools doc/konsolekalendar
entry importwizard akonadi-import-wizard doc
entry akregator akregator doc
entry contactthemeeditor grantlee-editor doc/contactthemeeditor
entry headerthemeeditor grantlee-editor doc/headerthemeeditor
entry kaddressbook kaddressbook doc
entry kalarm kalarm doc
entry kleopatra kleopatra doc/kleopatra
entry kwatchgnupg kleopatra doc/kwatchgnupg
entry kmail2 kmail doc/kmail2
entry akonadi_archivemail_agent kmail doc/akonadi_archivemail_agent
entry akonadi_followupreminder_agent kmail doc/akonadi_followupreminder_agent
entry akonadi_sendlater_agent kmail doc/akonadi_sendlater_agent
entry ktnef kmail ktnef/doc
entry knotes knotes doc/knotes
entry akonadi_notes_agent knotes doc/akonadi_notes_agent
entry kontact kontact doc/kontact
entry korganizer korganizer doc
entry pimsettingexporter pim-data-exporter doc
entry sieveeditor pim-sieve-editor doc
entry kioslave5/ldap kldap kioslave/doc/ldap
entry kioslave5/pop3 kdepim-runtime doc/pop3

# kdesdk
entry cervisia cervisia doc
entry kapptemplate kapptemplate doc
entry kcachegrind kcachegrind doc
entry kompare kompare doc
entry lokalize lokalize doc
entry poxml poxml doc
entry scripts kde-dev-scripts doc
entry umbrello umbrello doc

# kdeadmin
entry kcontrol5/kcron kcron doc/kcontrol5
entry ksystemlog ksystemlog doc

# kdeutils
entry kcontrol/blockdevices kdf doc/kcontrol
entry ark ark doc
entry kbackup kbackup doc/en
entry kcalc kcalc doc
entry kcharselect kcharselect doc
entry kdf kdf doc/app
entry kfloppy kfloppy doc
entry kgpg kgpg doc
entry filelight filelight doc
entry kteatime kteatime doc
entry ktimer ktimer doc
entry kwallet5 kwalletmanager doc
entry sweeper sweeper doc

# kdemultimedia
entry dragonplayer dragon doc
entry elisa elisa doc
entry juk juk doc
entry k3b k3b doc
entry kamoso kamoso doc
entry kdenlive kdenlive doc
entry kcontrol/cddbretrieval5 libkcddb kcmcddb/doc
entry kioslave5/audiocd audiocd-kio doc
entry kcontrol/kcmaudiocd audiocd-kio kcmaudiocd/doc
entry kmix kmix doc
entry kwave kwave doc/en

# kdenetwork
entry kdeconnect-kde kdeconnect-kde doc
entry kget kget doc
entry kioslave5 kio-extras doc/kioslave
entry kopete kopete doc
entry krdc krdc doc
entry krfb krfb doc

# kdeaccessibility
entry kmag kmag doc
entry kmousetool kmousetool doc
entry kmouth kmouth doc

# kdewebdev
entry kimagemapeditor kimagemapeditor doc

# kdevelop
entry kdevelop kdevelop doc/kdevelop

# calligra
entry calligra calligra doc/calligra
entry sheets calligra doc/sheets
entry stage calligra doc/stage
entry calligraplan calligraplan doc
entry kexi kexi doc/kexi

# extragear-base
entry kcontrol/wacomtablet wacomtablet doc/user

# extragear-edu
entry gcompris gcompris docs/docbook
entry labplot2 labplot doc

# extragear-graphics
entry kxstitch kxstitch doc
entry SymbolEditor symboleditor doc

# extragear-multimedia
entry kmplayer kmplayer doc

# extragear-network
entry kioslave5/gdrive kio-gdrive doc
entry kioslave5/s3 kio-s3 doc
entry konversation konversation doc
entry ktorrent ktorrent doc
entry smb4k smb4k doc

# extragear-office
entry kbibtex kbibtex doc
entry kmymoney kmymoney doc
entry tellico tellico doc

# extragear-utils
entry kdiff3 kdiff3 doc/en
entry kronometer kronometer doc
entry krusader krusader doc/handbook
entry okteta okteta doc
entry symmy symmy doc

# extragear-sdk
entry kdesvn kdesvn doc

# This is just a placeholder because the script
# is dumb and forgets to process the last line if there is not an empty line at the end
# having this at the end we make sure that is not a problem :-)
