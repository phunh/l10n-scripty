# -*- coding: utf-8 -*-
import os,sys
import subprocess
import Editor
import Project

def doCompile():
    if not Editor.isValid() or Editor.currentFile=='': return
    lang=Project.targetLangCode()
    (path, pofilename)=os.path.split(Editor.currentFile())
    (package, ext)=os.path.splitext(pofilename)
    xdgdirs = subprocess.check_output('qtpaths --paths GenericDataLocation', shell=True).split(':')
    for dir in xdgdirs:
        canWrite = os.access(dir, os.W_OK | os.X_OK)
        if canWrite:
            subprocess.check_output('mkdir --parents '+dir+'/locale/%s/LC_MESSAGES'  % lang, shell=True)
            # TODO run _qt.po files though lrelease to get a .qm
            subprocess.check_output('msgfmt -o '+dir+'/locale/%s/LC_MESSAGES/%s.mo %s' % (lang, package, Editor.currentFile()), shell=True)
            break

doCompile()
